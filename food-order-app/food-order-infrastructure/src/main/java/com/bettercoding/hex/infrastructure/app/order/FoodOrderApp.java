package com.bettercoding.hex.infrastructure.app.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication(scanBasePackages = "com.bettercoding.hex")
public class FoodOrderApp {
    public static void main(String[] args) {
        SpringApplication.run(FoodOrderApp.class, args);
    }

    @RestController
    public static class HelloRestController {
        @GetMapping("/appName")
        public ResponseEntity<String> getAppName() {
            return ResponseEntity.ok("FoodOrderApp");
        }
    }
}
