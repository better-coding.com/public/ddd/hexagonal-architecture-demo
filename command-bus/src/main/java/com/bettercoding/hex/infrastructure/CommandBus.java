package com.bettercoding.hex.infrastructure;

import com.bettercoding.hex.infrastructure.command.Command;

public interface CommandBus {
    void fire(Command command);

    void handle(Command command);
}
