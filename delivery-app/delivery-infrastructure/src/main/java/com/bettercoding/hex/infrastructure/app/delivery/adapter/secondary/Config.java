package com.bettercoding.hex.infrastructure.app.delivery.adapter.secondary;

import com.bettercoding.hex.domain.delivery.DeliveryFacade;
import com.bettercoding.hex.domain.delivery.port.secondary.OrderDetails;
import com.bettercoding.hex.domain.delivery.port.secondary.OrderNotification;
import com.bettercoding.hex.infrastructure.CommandBus;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration("config-delivery-domain")
class Config {
    @Bean
    DeliveryFacade deliveryFacade(OrderDetails orderDetails, OrderNotification orderNotification) {
        return new DeliveryFacade(orderDetails, orderNotification);
    }

    @Bean("delivery-order-details")
    OrderDetails orderDetails(@Qualifier("food-order-service-delivery-rest-template") RestTemplate restTemplate, @Value("${foodOrderEndpointUrl}") String foodOrderEndpointUrl) {
        return new FoodOrderDetailsRestAdapter(restTemplate, foodOrderEndpointUrl);
    }

    @Bean
    OrderNotification deliveryOrderNotification(CommandBus commandBus) {
        return new OrderNotificationAdapter(commandBus);
    }

    @Bean("food-order-service-delivery-rest-template")
    RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }
}
