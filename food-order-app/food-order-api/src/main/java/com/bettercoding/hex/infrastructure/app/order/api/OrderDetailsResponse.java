package com.bettercoding.hex.infrastructure.app.order.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetailsResponse {
    int orderId;
    String dishName;
    String address;
}
