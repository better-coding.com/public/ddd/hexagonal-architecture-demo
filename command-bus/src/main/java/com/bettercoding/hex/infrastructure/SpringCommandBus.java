package com.bettercoding.hex.infrastructure;

import com.bettercoding.hex.infrastructure.command.Command;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
@Slf4j
@RequiredArgsConstructor
class SpringCommandBus implements CommandBus {
    private final ApplicationEventPublisher eventPublisher;
    private final ApplicationContext applicationContext;

    @Override
    public void fire(Command command) {
        log.info("before fire command: [{}]", command);
        eventPublisher.publishEvent(command);
        log.info("after fire command: [{}]", command);
    }

    @EventListener
    @Override
    public void handle(Command command) {
        log.info("before handle command: [{}]", command);
        Collection<CommandHandler> commandHandlers = new ArrayList<>();
        for (String beanName : applicationContext.getBeanNamesForType(CommandHandler.class)) {
            CommandHandler commandHandler = applicationContext.getBean(beanName, CommandHandler.class);
            commandHandlers.add(commandHandler);
        }

        CommandHandler commandHandler = commandHandlers.stream()
                .filter(it -> it.getHandledCommand() == command.getClass())
                .findFirst()
                .orElseThrow();
        commandHandler.handle(command);
        log.info("after handle command: [{}]", command);
    }
}
