package com.bettercoding.hex.domain.restaurant.port.secondary;

public interface OrderNotification {
    void orderReady(int orderId);
}
