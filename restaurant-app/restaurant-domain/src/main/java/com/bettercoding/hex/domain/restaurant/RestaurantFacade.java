package com.bettercoding.hex.domain.restaurant;

import com.bettercoding.hex.domain.restaurant.port.primary.CookCommandService;
import com.bettercoding.hex.domain.restaurant.port.secondary.OrderDetails;
import com.bettercoding.hex.domain.restaurant.port.secondary.OrderNotification;
import lombok.Getter;


@Getter
public class RestaurantFacade {
    private final CookCommandService cookCommandService;

    public RestaurantFacade(OrderDetails orderDetails, OrderNotification orderNotification) {
        cookCommandService = new CookCommandServiceImpl(orderDetails, orderNotification);
    }
}
