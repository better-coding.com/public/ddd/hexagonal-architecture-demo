package com.bettercoding.hex.infrastructure.app.order.config;

import com.bettercoding.hex.domain.order.port.primary.FoodOrderCommandService;
import com.bettercoding.hex.infrastructure.CommandHandler;
import com.bettercoding.hex.infrastructure.command.MarkOrderAsReadyToDeliveryCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
class MarkOrderAsReadyToDeliveryCommandHandler implements CommandHandler<MarkOrderAsReadyToDeliveryCommand> {
    private final FoodOrderCommandService foodOrderCommandService;

    @Override
    public void handle(MarkOrderAsReadyToDeliveryCommand command) {
        foodOrderCommandService.markAsReadyToDelivery(command.getOrderId());
    }

    @Override
    public Class<?> getHandledCommand() {
        return MarkOrderAsReadyToDeliveryCommand.class;
    }
}
