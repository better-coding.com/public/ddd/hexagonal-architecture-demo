package com.bettercoding.hex.domain.delivery;

import com.bettercoding.hex.domain.delivery.port.primary.DeliveryCommandService;
import com.bettercoding.hex.domain.delivery.port.secondary.OrderDetails;
import com.bettercoding.hex.domain.delivery.port.secondary.OrderNotification;
import lombok.Getter;

@Getter
public class DeliveryFacade {
    private final DeliveryCommandService deliveryCommandService;

    public DeliveryFacade(OrderDetails orderDetails, OrderNotification orderNotification) {
        deliveryCommandService = new DeliveryCommandServiceImpl(orderDetails, orderNotification);
    }
}
