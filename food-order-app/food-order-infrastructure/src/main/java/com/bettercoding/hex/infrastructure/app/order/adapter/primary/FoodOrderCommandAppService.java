package com.bettercoding.hex.infrastructure.app.order.adapter.primary;


import com.bettercoding.hex.domain.order.OrderFacade;
import com.bettercoding.hex.domain.order.port.primary.FoodOrderCommandService;
import lombok.experimental.Delegate;
import org.springframework.stereotype.Service;

@Service
class FoodOrderCommandAppService implements FoodOrderCommandService {
    @Delegate
    private final FoodOrderCommandService delegate;

    public FoodOrderCommandAppService(OrderFacade orderFacade) {
        delegate = orderFacade.getFoodOrderCommandService();
    }
}
