package com.bettercoding.hex.domain.order.port.primary;

import com.bettercoding.hex.domain.order.port.shared.OrderDto;

public interface FoodOrderQueryService {
    OrderDto getOrderDetails(int orderId);
}
