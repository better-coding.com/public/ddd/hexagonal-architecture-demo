package com.bettercoding.hex.infrastructure.app.order.adapter.secondary;

import com.bettercoding.hex.domain.order.port.secondary.OrderStore;
import com.bettercoding.hex.domain.order.port.shared.OrderDto;
import com.bettercoding.hex.domain.order.port.shared.OrderState;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
class InMemoryOrderStore implements OrderStore {
    Map<Integer, OrderDto> memory = new HashMap<>();

    @Override
    public void save(OrderDto orderDto) {
        log.info("save: orderId[{}]", orderDto.getOrderId());
        memory.put(orderDto.getOrderId(), orderDto);
    }

    @Override
    public OrderDto load(int orderId) {
        log.info("load: orderId[{}]", orderId);
        return memory.get(orderId);
    }

    @Override
    public Collection<OrderDto> findByState(OrderState orderState) {
        return memory.values().stream().filter(it -> it.getOrderState() == orderState).collect(Collectors.toList());
    }
}
