package com.bettercoding.hex.test.domain.order;

import com.bettercoding.hex.domain.order.OrderFacade;
import com.bettercoding.hex.domain.order.port.primary.FoodOrderCommandService;
import com.bettercoding.hex.domain.order.port.primary.FoodOrderQueryService;
import com.bettercoding.hex.domain.order.port.secondary.Logistics;
import com.bettercoding.hex.domain.order.port.secondary.OrderStore;
import com.bettercoding.hex.domain.order.port.shared.OrderDto;
import com.bettercoding.hex.domain.order.port.shared.OrderState;
import lombok.Setter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;


class FoodOrderDomainTest {
    @Test
    public void createOrderTest() {
        OrderStore oderStore = new InMemoryOrderStore();

        Logistics logistics = new LogisticsMock();

        OrderFacade orderFacade = new OrderFacade(oderStore, null);
        FoodOrderCommandService foodOrderCommandService = orderFacade.getFoodOrderCommandService();
        FoodOrderQueryService foodOrderQueryService = orderFacade.getFoodOrderQueryService();

        int orderId = foodOrderCommandService.createOrder("Burger", "ul. Balonowa");
        Assertions.assertNotEquals(0, orderId);
        OrderDto orderDetails = foodOrderQueryService.getOrderDetails(orderId);
        Assertions.assertEquals(OrderState.NEW, orderDetails.getOrderState());
    }

    @Test
    public void logisticsTest() {
        LogisticsMock logistics = new LogisticsMock();

        OrderFacade orderFacade = new OrderFacade(new InMemoryOrderStore(), logistics);
        logistics.setOrderFacade(orderFacade);
        //When create order
        int orderId = orderFacade.getFoodOrderCommandService().createOrder("Burger", "Wall street");

        //Then
        Assertions.assertNotEquals(0, orderId);
        Assertions.assertEquals(OrderState.NEW, orderFacade.getFoodOrderQueryService().getOrderDetails(orderId).getOrderState());

        //When cron triggers makeOrders
        orderFacade.getFoodOrderCommandService().makeOrders();

        //Then
        Assertions.assertEquals(OrderState.READY_TO_DELIVERY, orderFacade.getFoodOrderQueryService().getOrderDetails(orderId).getOrderState());

        //When cron triggers makeOrders
        orderFacade.getFoodOrderCommandService().makeOrders();

        //Then
        Assertions.assertEquals(OrderState.DELIVERED, orderFacade.getFoodOrderQueryService().getOrderDetails(orderId).getOrderState());

    }


    private static class InMemoryOrderStore implements OrderStore {
        private final Map<Integer, OrderDto> memory = new HashMap<>();

        @Override
        public void save(OrderDto orderDto) {
            System.out.println("OrderStore::save: " + orderDto.getOrderId());
            memory.put(orderDto.getOrderId(), orderDto);
        }

        @Override
        public OrderDto load(int orderId) {
            System.out.println("OrderStore::load: " + orderId);
            return memory.get(orderId);
        }

        @Override
        public Collection<OrderDto> findByState(OrderState orderState) {
            return memory.values().stream().filter(it -> it.getOrderState() == orderState).collect(Collectors.toList());
        }
    }


    private static class LogisticsMock implements Logistics {
        @Setter
        private OrderFacade orderFacade;

        @Override
        public void prepareOrder(int orderId) {
            orderFacade.getFoodOrderCommandService().markAsReadyToDelivery(orderId);
        }

        @Override
        public void deliver(int orderId) {
            orderFacade.getFoodOrderCommandService().markAsDelivered(orderId);
        }
    }
}
