package com.bettercoding.hex.infrastructure.app.order.adapter.primary.rest;

import com.bettercoding.hex.domain.order.port.primary.FoodOrderQueryService;
import com.bettercoding.hex.domain.order.port.shared.OrderDto;
import com.bettercoding.hex.infrastructure.app.order.api.OrderDetailsResponse;
import lombok.RequiredArgsConstructor;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/food-orders")
@RequiredArgsConstructor
public class FoodOrderEndpoint {
    private final FoodOrderQueryService foodOrderQueryService;
    private final OrderDetailsMapper orderDetailsMapper = Mappers.getMapper(OrderDetailsMapper.class);

    @GetMapping("/{orderId}")
    public ResponseEntity<OrderDetailsResponse> getOrderDetails(@PathVariable("orderId") int orderId) {
        //TODO add validations
        OrderDto orderDto = foodOrderQueryService.getOrderDetails(orderId);
        OrderDetailsResponse orderDetailsResponse = orderDetailsMapper.toOrderDetailsResponse(orderDto);
        return ResponseEntity.ok(orderDetailsResponse);
    }

    @Mapper
    interface OrderDetailsMapper {
        OrderDetailsResponse toOrderDetailsResponse(OrderDto orderDto);
    }
}
