package com.bettercoding.hex.test.domain.delivery;

import com.bettercoding.hex.domain.delivery.DeliveryFacade;
import com.bettercoding.hex.domain.delivery.port.secondary.OrderDetails;
import com.bettercoding.hex.domain.delivery.port.secondary.OrderNotification;
import com.bettercoding.hex.domain.delivery.port.shared.OrderDetailsDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicBoolean;

class DeliveryDomainTest {

    @Test
    public void deliveryTest() {
        OrderDetails orderDetails = new OrderDetails() {
            @Override
            public OrderDetailsDto getOrderDetails(int orderId) {
                return new OrderDetailsDto(orderId, "Wall Street 1");
            }
        };

        AtomicBoolean notificationWasSent = new AtomicBoolean(false);
        OrderNotification orderNotification = new OrderNotification() {
            @Override
            public void orderDelivered(int orderId) {
                notificationWasSent.set(true);
            }
        };

        DeliveryFacade deliveryFacade = new DeliveryFacade(orderDetails, orderNotification);

        //When someone order delivery
        deliveryFacade.getDeliveryCommandService().deliverOrder(1);

        //Then
        Assertions.assertTrue(notificationWasSent.get());
    }
}
