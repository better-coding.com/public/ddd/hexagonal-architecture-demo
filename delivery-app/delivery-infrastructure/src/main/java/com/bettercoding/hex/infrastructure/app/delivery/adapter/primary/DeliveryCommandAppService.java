package com.bettercoding.hex.infrastructure.app.delivery.adapter.primary;


import com.bettercoding.hex.domain.delivery.DeliveryFacade;
import com.bettercoding.hex.domain.delivery.port.primary.DeliveryCommandService;
import lombok.experimental.Delegate;
import org.springframework.stereotype.Service;

@Service
class DeliveryCommandAppService implements DeliveryCommandService {
    @Delegate
    private final DeliveryCommandService delegate;

    public DeliveryCommandAppService(DeliveryFacade deliveryFacade) {
        delegate = deliveryFacade.getDeliveryCommandService();
    }
}
