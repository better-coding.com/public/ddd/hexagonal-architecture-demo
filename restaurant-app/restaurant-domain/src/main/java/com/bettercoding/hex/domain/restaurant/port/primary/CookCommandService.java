package com.bettercoding.hex.domain.restaurant.port.primary;

public interface CookCommandService {
    void prepareOrder(int fooOrderId);
}
