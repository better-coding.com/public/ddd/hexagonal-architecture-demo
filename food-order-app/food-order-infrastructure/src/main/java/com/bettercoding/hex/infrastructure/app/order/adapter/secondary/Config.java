package com.bettercoding.hex.infrastructure.app.order.adapter.secondary;

import com.bettercoding.hex.domain.order.OrderFacade;
import com.bettercoding.hex.domain.order.port.secondary.Logistics;
import com.bettercoding.hex.domain.order.port.secondary.OrderStore;
import com.bettercoding.hex.infrastructure.CommandBus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration("config-food-order-domain")
class Config {
    @Bean
    OrderFacade orderFacade(OrderStore orderStore, Logistics logistics) {
        return new OrderFacade(orderStore, logistics);
    }

    @Bean
    OrderStore orderStore() {
        return new InMemoryOrderStore();
    }

    @Bean
    Logistics logistics(CommandBus commandBus) {
        return new TrueLogistics(commandBus);
    }
}
