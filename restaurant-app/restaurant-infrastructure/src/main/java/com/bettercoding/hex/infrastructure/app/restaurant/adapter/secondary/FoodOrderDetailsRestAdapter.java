package com.bettercoding.hex.infrastructure.app.restaurant.adapter.secondary;

import com.bettercoding.hex.domain.restaurant.port.secondary.OrderDetails;
import com.bettercoding.hex.domain.restaurant.port.shared.OrderDetailsDto;
import com.bettercoding.hex.infrastructure.app.order.api.OrderDetailsResponse;
import lombok.RequiredArgsConstructor;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import org.springframework.web.client.RestTemplate;

@RequiredArgsConstructor
class FoodOrderDetailsRestAdapter implements OrderDetails {

    private final RestTemplate restTemplate;
    private final String foodOrderEndpointUrl;
    private final OrderDetailMapper mapper = Mappers.getMapper(OrderDetailMapper.class);

    @Override
    public OrderDetailsDto getOrderDetails(int orderId) {
        OrderDetailsResponse orderDetailsResponse = restTemplate.getForObject(foodOrderEndpointUrl, OrderDetailsResponse.class, orderId);
        return mapper.toOrderDetailsDto(orderDetailsResponse);
    }

    @Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR)
    interface OrderDetailMapper {
        OrderDetailsDto toOrderDetailsDto(OrderDetailsResponse orderDetailsResponse);
    }
}
