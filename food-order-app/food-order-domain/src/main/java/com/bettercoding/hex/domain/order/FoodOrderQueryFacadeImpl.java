package com.bettercoding.hex.domain.order;

import com.bettercoding.hex.domain.order.port.primary.FoodOrderQueryService;
import com.bettercoding.hex.domain.order.port.secondary.OrderStore;
import com.bettercoding.hex.domain.order.port.shared.OrderDto;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class FoodOrderQueryFacadeImpl implements FoodOrderQueryService {
    private final OrderStore orderStore;

    @Override
    public OrderDto getOrderDetails(int orderId) {
        return orderStore.load(orderId);
    }
}
