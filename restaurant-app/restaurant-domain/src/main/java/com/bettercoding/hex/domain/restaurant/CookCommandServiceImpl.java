package com.bettercoding.hex.domain.restaurant;

import com.bettercoding.hex.domain.restaurant.port.primary.CookCommandService;
import com.bettercoding.hex.domain.restaurant.port.secondary.OrderDetails;
import com.bettercoding.hex.domain.restaurant.port.secondary.OrderNotification;
import com.bettercoding.hex.domain.restaurant.port.shared.OrderDetailsDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
class CookCommandServiceImpl implements CookCommandService {
    private final OrderDetails orderDetails;
    private final OrderNotification orderNotification;

    @Override
    public void prepareOrder(int orderId) {
        OrderDetailsDto od = orderDetails.getOrderDetails(orderId);
        log.info("Preparing dish {}", od.getDishName());
        orderNotification.orderReady(od.getOrderId());
    }
}
