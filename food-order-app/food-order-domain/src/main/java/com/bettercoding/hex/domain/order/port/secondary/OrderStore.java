package com.bettercoding.hex.domain.order.port.secondary;

import com.bettercoding.hex.domain.order.port.shared.OrderDto;
import com.bettercoding.hex.domain.order.port.shared.OrderState;

import java.util.Collection;

public interface OrderStore {
    void save(OrderDto orderDto);

    OrderDto load(int orderId);

    Collection<OrderDto> findByState(OrderState orderState);
}
