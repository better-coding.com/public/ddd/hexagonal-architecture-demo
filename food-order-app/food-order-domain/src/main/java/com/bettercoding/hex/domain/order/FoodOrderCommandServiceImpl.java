package com.bettercoding.hex.domain.order;

import com.bettercoding.hex.domain.order.port.primary.FoodOrderCommandService;
import com.bettercoding.hex.domain.order.port.secondary.Logistics;
import com.bettercoding.hex.domain.order.port.secondary.OrderStore;
import com.bettercoding.hex.domain.order.port.shared.OrderDto;
import com.bettercoding.hex.domain.order.port.shared.OrderState;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;

@Slf4j
@RequiredArgsConstructor
class FoodOrderCommandServiceImpl implements FoodOrderCommandService {
    private final OrderStore orderStore;
    private final Logistics logistics;

    @Override
    public int createOrder(String dishName, String address) {
        Order order = Order.OrderFactory.getInstance().createOrder(dishName, address);
        orderStore.save(Order.OrderFactory.getInstance().toOrderDto(order));
        return order.getOrderId();
    }

    @Override
    public void markAsReadyToDelivery(int orderId) {
        changeOrderState(orderId, OrderState.READY_TO_DELIVERY);
    }

    @Override
    public void markAsDelivered(int orderId) {
        changeOrderState(orderId, OrderState.DELIVERED);
    }

    @Override
    public void makeOrders() {
        Collection<OrderDto> newOrders = orderStore.findByState(OrderState.NEW);
        Collection<OrderDto> readyToDeliveryOrders = orderStore.findByState(OrderState.READY_TO_DELIVERY);

        newOrders.forEach(order -> {
            log.info("Preparing order {}", order.getOrderId());
            changeOrderState(order.getOrderId(), OrderState.SENT_TO_RESTAURANT);
            logistics.prepareOrder(order.getOrderId());
        });

        readyToDeliveryOrders.forEach(order -> {
            log.info("Delivering order {}", order.getOrderId());
            logistics.deliver(order.getOrderId());
        });
    }

    private void changeOrderState(int orderId, OrderState newOrderState) {
        OrderDto orderDto = orderStore.load(orderId);
        Order order = Order.OrderFactory.getInstance().from(orderDto);
        order.changeState(newOrderState);
        orderStore.save(Order.OrderFactory.getInstance().toOrderDto(order));
    }
}
