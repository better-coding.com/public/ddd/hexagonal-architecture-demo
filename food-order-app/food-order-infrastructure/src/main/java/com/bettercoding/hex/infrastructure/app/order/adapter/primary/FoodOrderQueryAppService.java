package com.bettercoding.hex.infrastructure.app.order.adapter.primary;


import com.bettercoding.hex.domain.order.OrderFacade;
import com.bettercoding.hex.domain.order.port.primary.FoodOrderQueryService;
import lombok.experimental.Delegate;
import org.springframework.stereotype.Service;

@Service
class FoodOrderQueryAppService implements FoodOrderQueryService {
    @Delegate
    private final FoodOrderQueryService delegate;

    public FoodOrderQueryAppService(OrderFacade orderFacade) {
        delegate = orderFacade.getFoodOrderQueryService();
    }
}
